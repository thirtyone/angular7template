import {InjectionToken} from '@angular/core';
import {AppInterface} from '../shared/interfaces/app.interface';
import {environment} from '../../environments/environment.staging';

export let APP_CONFIG = new InjectionToken('app.config');

const version = {
  v1_0_0: 'v1.0.0/',
  v1_0_1: 'v1.0.0/'
};
// @ts-ignore
export const AppConfig: AppInterface = {

  endpoints: {
    login: `${version.v1_0_0}franchisor/auth/login`,
    register: `${version.v1_0_0}franchisor/auth/register`,
    samples:  `${version.v1_0_0}samples`,
  },
  api_url: environment.api_url,


};
