import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ArticleComponent} from './article.component';
import {ArticleRouter, ArticleRoutingComponents} from './article.router';

@NgModule({
     declarations: [
          ArticleComponent,
          ArticleRoutingComponents
     ],
     imports: [
          CommonModule,
          ArticleRouter
     ]
})
export class ArticleModule {
}
