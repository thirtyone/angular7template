import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ShowComponent} from './show/show.component';
import {ListComponent} from './list/list.component';
import {ArticleComponent} from './article.component';

const routes: Routes = [{
     path: '',
     component: ArticleComponent,
     children: [
          {
               path: '',
               component: ListComponent,
          },
          {
               path: ':slug',
               component: ShowComponent
          }

     ]
}];

@NgModule({
     imports: [RouterModule.forChild(routes)],
     exports: [RouterModule],
})
export class ArticleRouter {
}

export const ArticleRoutingComponents = [ArticleComponent, ListComponent, ShowComponent];
