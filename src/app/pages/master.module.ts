import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MasterComponent} from '../cores/layouts/master/master.component';
import {MasterRouter, MasterRoutingComponents} from './master.router';

@NgModule({
     declarations: [
          MasterComponent,
          MasterRoutingComponents

     ],
     imports: [
          CommonModule,
          MasterRouter
     ]
})
export class MasterModule {
}
