import {Component, OnInit} from '@angular/core';
@Component({
     selector: 'app-article',
     template: '<router-outlet></router-outlet>'
})
export class SampleComponent implements OnInit {

     constructor() {
     }

     ngOnInit() {

     }
}
