import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SampleComponent} from './sample.component';
import {SampleRouter, SampleRoutingComponents} from './sample.router';

@NgModule({
     declarations: [
          SampleComponent,
          SampleRoutingComponents
     ],
     imports: [
          CommonModule,
          SampleRouter
     ]
})
export class SampleModule {
}
