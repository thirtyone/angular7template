import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ShowComponent} from './show/show.component';
import {ListComponent} from './list/list.component';
import {SampleComponent} from './sample.component';

const routes: Routes = [{
     path: '',
     component: SampleComponent,
     children: [
          {
               path: '',
               component: ListComponent,
          },
          {
               path: ':slug',
               component: ShowComponent
          }

     ]
}];

@NgModule({
     imports: [RouterModule.forChild(routes)],
     exports: [RouterModule],
})
export class SampleRouter {
}

export const SampleRoutingComponents = [SampleComponent, ListComponent, ShowComponent];
