import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MasterComponent} from '../cores/layouts/master/master.component';
import {HomeComponent} from './home/home.component';


const routes: Routes = [{
     path: '',
     component: MasterComponent,
     children: [

          {
               path: '',
               component: HomeComponent
          },
          {
               path: 'articles',
               loadChildren: () => import('../pages/article/article.module')
                    .then(m => m.ArticleModule)
          },
          {
               path: 'samples',
               loadChildren: () => import('../pages/sample/sample.module')
                    .then(m => m.SampleModule)
          },
     ]
}];

@NgModule({
     imports: [RouterModule.forChild(routes)],
     exports: [RouterModule],
})
export class MasterRouter {
}

export const MasterRoutingComponents = [MasterComponent, HomeComponent];
