import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, tap} from "rxjs/operators";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const auth = JSON.parse(localStorage.getItem('auth'));
    if (auth && auth.access_token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${auth.access_token}`
        }
      });
    }

    return next.handle(request).pipe(
      tap(success => {
        // @ts-ignore
        if (success.body && success.body.data) {
          // @ts-ignore
          const response = success.body;
          if (response.data && response.data.access_token) {
            localStorage.setItem('auth', JSON.stringify(response.data));
          }
        }
      }),
    );
  }
}
