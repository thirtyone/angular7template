import {EventEmitter, Injectable, Output} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {AppConfig} from '../../../config/app.config';
import {catchError, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';


const appConfig = AppConfig;

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  public action;
  @Output() auth: EventEmitter<any> = new EventEmitter();

  constructor(private httpClient: HttpClient) {

  }
  //put, delete, post, get

  login(data) {


    // @ts-ignore
    this.action = this.httpClient.post(appConfig.endpoints.login, data).pipe(map(success => {
      // @ts-ignore
      return success.data;
    }), catchError(fail => {

      return throwError(fail.error.data);
    }));
    return this.action;
  }

  //
  // register(data) {
  //
  //   // this.action = this.httpClient.post(AppConfig.endpoints.register, data).pipe(map(success => {
  //   //     //
  //   //     //
  //   //     // // @ts-ignore
  //   //     // if (returnValue && returnValue.access_token) {
  //   //     //     // store user details and jwt token in local storage to keep user logged in between page refreshes
  //   //     //     this.authUser.emit(returnValue);
  //   //     //     localStorage.setItem('authUser', JSON.stringify(returnValue));
  //   //     // }
  //   //
  //   //     // @ts-ignore
  //   //     // return returnValue.user;
  //   //
  //   // }));
  //   // return this.action;
  // }

  logout(data) {


    // this.action = this.httpClient.post(AppConfig.endpoints.logout, data).pipe(map(success => {
    //
    //     // this.authUser.emit(false);
    //     // localStorage.removeItem('authUser');
    //
    // }));
    //
    // return this.action;
  }

}
