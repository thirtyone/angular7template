import {EventEmitter, Injectable, Output} from '@angular/core';
import {Observable, throwError} from 'rxjs';
// @ts-ignore
import {AppConfig} from '../../../config/app.config';
import {catchError, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';


const appConfig = AppConfig;

@Injectable({
  providedIn: 'root'
})

export class SampleService {
  public action;
  @Output() auth: EventEmitter<any> = new EventEmitter();

  constructor(private httpClient: HttpClient) {

  }

  //put, delete, post, get

  list(data) {

    // @ts-ignore
    this.action = this.httpClient.get(appConfig.endpoints.samples, {
      params: data
    }).pipe(map(success => {
      // @ts-ignore

      return success.data;
    }), catchError(fail => {

      return throwError(fail.error.data);
    }));
    return this.action;
  }


  show(id) {

    // @ts-ignore
    this.action = this.httpClient.get(appConfig.endpoints.samples + `/${id}`).pipe(map(success => {
      // @ts-ignore
      return success.data;
    }), catchError(fail => {


      return throwError(fail.error.data);
    }));
    return this.action;
  }

  store(data) {

    // @ts-ignore
    this.action = this.httpClient.post(appConfig.endpoints.samples, data).pipe(map(success => {
      // @ts-ignore
      return success.data;
    }), catchError(fail => {
      return throwError(fail.error.data);
    }));
    return this.action;
  }


  update(id,data) {

    // @ts-ignore
    this.action = this.httpClient.put(appConfig.endpoints.samples + `/${id}`, data).pipe(map(success => {
      // @ts-ignore
      return success.data;
    }), catchError(fail => {

      return throwError(fail.error.data);
    }));
    return this.action;
  }


  delete(id) {

    // @ts-ignore
    this.action = this.httpClient.delete(appConfig.endpoints.samples + `/${id}`).pipe(map(success => {
      // @ts-ignore
      return success.data;
    }), catchError(fail => {

      return throwError(fail.error.data);
    }));
    return this.action;
  }





}
