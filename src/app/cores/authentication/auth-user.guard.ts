import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';


@Injectable({providedIn: 'root'})
export class AuthUserGuard implements CanActivate {

  public data;

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('auth')) {
      return true;
    }


    const data = {
      url: state.url,
      message: 'Please login!',
      status: true
    };

    // not logged in so redirect to login page with the return url
    // @ts-ignore
    localStorage.setItem('redirect', JSON.stringify(data));
    this.router.navigate(['/login']);
    return false;
  }
}
