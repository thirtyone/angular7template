import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';


@Injectable({providedIn: 'root'})
export class GuestOnlyGuard implements CanActivate {

    public data;

    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!localStorage.getItem('authUser')) {
            return true;

        }

        this.router.navigate(['/']);
        return false;

    }
}

