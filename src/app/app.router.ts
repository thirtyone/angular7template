import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
     {
          path: '',
          loadChildren: () => import('../app/pages/master.module')
               .then(m => m.MasterModule)
     },

];

@NgModule({
     imports: [RouterModule.forRoot(routes)],
     exports: [RouterModule]
})
export class AppRouter {

}

export const AppRoutingComponents = [];
