import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {AppRouter, AppRoutingComponents} from './app.router';
import {AppComponent} from './app.component';
import {JwtInterceptor} from './cores/interceptors/jwt.interceptor';
import {AuthErrorHandler} from './cores/handlers/auth-error.handler';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {APIInterceptor} from './cores/interceptors/api.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    AppRoutingComponents,
  ],
  imports: [
    BrowserModule,
    AppRouter,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIInterceptor,
      multi: true,
    },
    {
      provide: ErrorHandler,
      useClass: AuthErrorHandler
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
