export const environment = {
  production: true,
  url: '127.0.0.0:8000',
  api_url: 'http://127.0.0.1:8000/api',
};
